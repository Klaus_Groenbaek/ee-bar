package dk.groenbaek.ee.resturant.jpa;

import dk.groenbaek.ee.resturant.jpa.datagenerators.UniqueIdGenerator;
import dk.groenbaek.ee.resturant.jpa.entity.AbstractEntityBase;

import javax.inject.Inject;
import javax.persistence.PrePersist;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;
import java.util.UUID;

/**
 * It seems that Beans are not automatically validated, so we have to do it ourselves...
 * Also assign a UUID to every entity so we don't expose PK in the frontend
 * @author Klaus Groenbaek
 *         Created 21/05/2017.
 */
public class EntityBaseEntityListener {

    @Inject
    Validator validator;

    @Inject
    UniqueIdGenerator idGenerator;

    @PrePersist
    public void validate(AbstractEntityBase entity) {
        Set<ConstraintViolation<AbstractEntityBase>> violations = validator.validate(entity);
        if (!violations.isEmpty()) {
            ConstraintViolation<AbstractEntityBase> firstViolation = violations.iterator().next();
            throw new IllegalStateException(firstViolation.getPropertyPath() + ": " + firstViolation.getMessage());
        }

        if (entity.getUniqueId() == null) {
            // create a URL friendly uniqueId

            entity.setUniqueId(idGenerator.createUniqueId());
        }
    }
}
