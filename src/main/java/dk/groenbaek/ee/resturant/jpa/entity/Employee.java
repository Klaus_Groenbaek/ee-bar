package dk.groenbaek.ee.resturant.jpa.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.LocalDate;


/**
 * @author Klaus Groenbaek
 *         Created 20/05/2017.
 */
@Entity
@DiscriminatorColumn(name="E_TYPE", discriminatorType=DiscriminatorType.CHAR)
@Inheritance(strategy = InheritanceType.JOINED)
@Getter
@Setter
@Accessors(chain = true)
public abstract class Employee extends AbstractEntityBase {

    @Basic(optional = false)
    @Column(nullable = false)
    private String name;

    @Basic(optional = false)
    @Column(nullable = false)
    private LocalDate dob;

    @Basic(optional = false)
    @Column(nullable = false)
    @Size(min = 10)
    private String ssn;

    protected Employee() {
    }

}
