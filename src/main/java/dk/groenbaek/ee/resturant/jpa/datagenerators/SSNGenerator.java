package dk.groenbaek.ee.resturant.jpa.datagenerators;

import javax.annotation.ManagedBean;
import java.util.Random;

/**
 * @author Klaus Groenbaek
 *         Created 21/05/2017.
 */
//@ManagedBean
public class SSNGenerator {

    private Random rnd = new Random();

    String randomSSN() {

        return "" + rnd.nextInt(10) + rnd.nextInt(10) + rnd.nextInt(10) + "-"
                + rnd.nextInt(10) + rnd.nextInt(10) + "-" +
                rnd.nextInt(10) + rnd.nextInt(10) + rnd.nextInt(10) + rnd.nextInt(10);

    }

    public static void main(String[] args) {
        String ssn = new SSNGenerator().randomSSN();
        System.out.println("ssn = " + ssn);
    }
}
