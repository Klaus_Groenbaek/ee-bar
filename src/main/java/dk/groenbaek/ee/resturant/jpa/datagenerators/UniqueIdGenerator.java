package dk.groenbaek.ee.resturant.jpa.datagenerators;

import java.util.UUID;

/**
 * @author Klaus Groenbaek
 *         Created 22/05/2017.
 */
public class UniqueIdGenerator {

    public String createUniqueId() {
        UUID uuid = UUID.randomUUID();
        String uniqueId = Long.toHexString(uuid.getMostSignificantBits()) + Long.toHexString(uuid.getLeastSignificantBits());
        return uniqueId;
    }



}
