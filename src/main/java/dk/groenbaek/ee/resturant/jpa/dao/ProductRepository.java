package dk.groenbaek.ee.resturant.jpa.dao;

import dk.groenbaek.ee.resturant.jpa.entity.Product;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author Klaus Groenbaek
 *         Created 22/05/2017.
 */
public class ProductRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public Product findByName(String name) {
        List<Product> products = entityManager.createQuery("select p from Product p where p.name = :name", Product.class).setParameter("name", name).getResultList();
        if (products.isEmpty()) {
            return null;
        } else {
            return products.get(0);
        }
    }

    public List<Product> all() {
        return entityManager.createQuery("select p from Product p", Product.class).getResultList();
    }
}
