package dk.groenbaek.ee.resturant.jpa.datagenerators;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import dk.groenbaek.ee.resturant.jpa.dao.ProductRepository;
import dk.groenbaek.ee.resturant.jpa.entity.*;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * Class that populates the database with some sample data when the application starts up
 * @author Klaus Groenbaek
 *         Created 21/05/2017.
 */
@Startup()
@Singleton
public class DataPopulator {

    public static final long MAX_MANAGERS = 3;
    public static final long MAX_BARTENDERS = 10;
    public static final long MAX_WAITERS = 50;
    public static final long MAX_HOSTS = 10;

    private Random rnd = new Random();

    @Inject
    RandomEmployeeFactory employeeFactory;

    @PersistenceContext
    private EntityManager entityManager;

    @Inject
    private ProductRepository productRepository;


    @PostConstruct
    @Transactional
    @SneakyThrows(IOException.class)
    public void insertConfigurationsIfNotExist() {


        long count = entityManager.createQuery("select count(m) from Manager m", Long.class).getSingleResult();
        for (long i = count; i < MAX_MANAGERS; i++) {
            entityManager.persist(employeeFactory.createEmployee(25 + rnd.nextInt(30), Manager.class));
        }

        count = entityManager.createQuery("select count(b) from Bartender b", Long.class).getSingleResult();
        for (long i = count; i < MAX_BARTENDERS; i++) {
            Bartender bartender = employeeFactory.createEmployee(21 + rnd.nextInt(20), Bartender.class);
            bartender.setMultiplier(0.50 + rnd.nextDouble());
            entityManager.persist(bartender);
        }

        count = entityManager.createQuery("select count(w) from Waiter w", Long.class).getSingleResult();
        for (long i = count; i < MAX_WAITERS; i++) {
            Waiter waiter = employeeFactory.createEmployee(18 + rnd.nextInt(10), Waiter.class);
            waiter.setTableCapacity(3 + rnd.nextInt(5));
            entityManager.persist(waiter);
        }

        count = entityManager.createQuery("select count(h) from Host h", Long.class).getSingleResult();
        for (long i = count; i < MAX_HOSTS; i++) {
            entityManager.persist(employeeFactory.createEmployee(15 + rnd.nextInt(10), Host.class));
        }

        // now create 3 shifts
        ArrayList<ShiftConfiguration> shiftIds = Lists.newArrayList(
                new ShiftConfiguration("lunch", 1, 10, 1, 10, 5),
                new ShiftConfiguration("dinner", 2, 20, 2, 15, 5),
                new ShiftConfiguration("evening", 3, 4, 1, 20, 6));

        createShifts(shiftIds);

        // create products from classpath resource
        ObjectMapper objectMapper = new ObjectMapper();
        try (InputStream resourceAsStream = getClass().getResourceAsStream("/products.json")) {
            Product[] products = objectMapper.readValue(resourceAsStream, Product[].class);
            for (Product product : products) {
                Product found = productRepository.findByName(product.getName());
                if (found == null) {
                    entityManager.persist(product);
                }
            }
        }
    }

    public List<Shift> createShifts(ArrayList<ShiftConfiguration> shiftIds) {
        long count;List<Manager> managers = entityManager.createQuery("select m from Manager m", Manager.class).getResultList();
        List<Bartender> bartenders = entityManager.createQuery("select b from Bartender b", Bartender.class).getResultList();
        List<Waiter> waiters = entityManager.createQuery("select w from Waiter w", Waiter.class).getResultList();
        List<Host> hosts = entityManager.createQuery("select h from Host h", Host.class).getResultList();
        List<Shift> created = new ArrayList<>();
        for (ShiftConfiguration config : shiftIds) {
            count = entityManager.createQuery("select count(s) from Shift s where s.uniqueId = :uniqueId", Long.class)
                    .setParameter("uniqueId", config.name).getSingleResult();
            if (count == 0) {

                LocalDateTime start = LocalDateTime.now().with(ChronoField.HOUR_OF_DAY, config.startHour);
                Shift shift = new Shift()
                        .setStart(start.with(ChronoField.HOUR_OF_DAY, config.startHour).atZone(ZoneOffset.UTC).toInstant())
                        .setStop(start.plus(config.hoursDuration, ChronoUnit.HOURS).atZone(ZoneId.systemDefault()).toInstant());
                shift.setUniqueId(config.name);
                shift.setManager(managers.get(rnd.nextInt(managers.size())));

                // take a copy of the lists, and remove the employee when picked for the shift, since the same employee can't be assigned twice
                List<Bartender> remainingBartenders = new ArrayList<>(bartenders);
                List<Waiter> remainingWaiters = new ArrayList<>(waiters);
                List<Host> remainingHosts = new ArrayList<>(hosts);

                for (int i = 0; i < config.bartenders; i++) {
                    shift.addBartender(remainingBartenders.remove(rnd.nextInt(remainingBartenders.size())));
                }

                for (int i = 0; i < config.waiters; i++) {
                    shift.addWaiter(remainingWaiters.remove(rnd.nextInt(remainingWaiters.size())));
                }

                for (int i = 0; i < config.hosts; i++) {
                    shift.addHost(remainingHosts.remove(rnd.nextInt(remainingHosts.size())));
                }
                entityManager.persist(shift);
                entityManager.flush();  // catch constraint violations faster
                created.add(shift);
            }
        }
        return created;
    }

    @AllArgsConstructor
    public static class ShiftConfiguration {
        private final String name;
        private final int bartenders;
        private final int waiters;
        private final int hosts;
        private final int startHour;
        private final int hoursDuration;
    }


}
