package dk.groenbaek.ee.resturant.jpa.entity;

import dk.groenbaek.ee.resturant.jpa.JPAUtils;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Collection;

/**
 * @author Klaus Groenbaek
 *         Created 22/05/2017.
 */
@Entity
@Getter
@Setter
@Accessors(chain = true)
@Table(name = "BARORDER")   // order is a SQL keyword, as in "order by"
public class Order extends AbstractEntityBase {

    @JoinColumn(nullable = false)
    @ManyToOne(optional = false)
    private Shift shift;

    @JoinColumn(nullable = false)
    @ManyToOne(optional = false)
    private Bartender bartender;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    @Setter(AccessLevel.PRIVATE)
    private Collection<OrderLine> lineItems;

    public Collection<OrderLine> getLineItems() {
        return lineItems = JPAUtils.initCollection(lineItems);
    }

    /**
     * This method is intended to be used if the entire order (all orderLines) are sent as one unit.
     * It loads the existing orderLines (performing a query if the order is already persisted)
     * @param product the product
     * @param quantity the quantity of the product to order
     */
    public void addProduct(Product product, int quantity) {
        getLineItems().add(new OrderLine().setOrder(this).setProduct(product).setQuantity(quantity));
    }

    /**
     * calculates the total time to make the order.
     * @return the number of seconds to make the order
     */
    public int getTotalTimeToMake() {
        int total = 0;
        for (OrderLine lineItem : lineItems) {
            total += lineItem.getProduct().getTimeToMake() * lineItem.getQuantity();
        }
        return total;
    }

    public double getTotalPrice() {
        return JPAUtils.wrapForStream(lineItems).stream().mapToDouble(l-> l.getQuantity() * l.getProduct().getPrice()).sum();
    }
}
