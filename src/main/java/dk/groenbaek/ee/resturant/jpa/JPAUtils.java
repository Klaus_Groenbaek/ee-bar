package dk.groenbaek.ee.resturant.jpa;

import dk.groenbaek.ee.resturant.jpa.entity.MappingBartenderShift;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

/**
 * @author Klaus Groenbaek
 *         Created 21/05/2017.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class JPAUtils {

    public static <T> Collection<T> initCollection(Collection<T> collection) {
        if (collection == null) {
            collection = new ArrayList<>();
        }
        return collection;
    }

    /**
     * Stream does not work on EclipseLinks IndirectList, because of the way the JDK implements streaming on Vector, so we have to wrap the collection
     * if we want streaming to work
     */
    public static <T> Collection<T> wrapForStream(Collection<T> collection) {
        if (collection == null) {
            return new ArrayList<>();
        }
        if (collection instanceof Vector) {
            return new ArrayList<>(collection);
        }
        return collection;
    }
}
