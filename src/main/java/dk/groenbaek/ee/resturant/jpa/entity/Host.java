package dk.groenbaek.ee.resturant.jpa.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Collection;

/**
 * @author Klaus Groenbaek
 *         Created 20/05/2017.
 */
@Entity
@DiscriminatorValue("H")
public class Host extends Employee {

    @OneToMany
    private Collection<MappingHostShift> shifts;
}
