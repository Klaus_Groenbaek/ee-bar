package dk.groenbaek.ee.resturant.jpa.entity;

import dk.groenbaek.ee.resturant.jpa.JPAUtils;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * A shift, with references to the employees and the orders sold
 *
 * @author Klaus Groenbaek
 *         Created 20/05/2017.
 */

@Accessors(chain = true)
@Entity
public class Shift extends AbstractEntityBase {

    @Basic(optional = false)
    @Column(nullable = false)
    @Setter
    @Getter
    private Instant start;

    @Basic(optional = false)
    @Column(nullable = false)
    @Setter
    @Getter
    private Instant stop;

    @JoinColumn(nullable = false)
    @ManyToOne(optional = false, cascade = CascadeType.PERSIST)
    @Setter
    @Getter
    private Manager manager;

    @Size(min = 1, max = 5)
    @OneToMany(mappedBy = "shift", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<MappingBartenderShift> bartenderMappings;

    @Size(min = 1, max = 3)
    @OneToMany(mappedBy = "shift", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<MappingHostShift> hostMapping;

    @Size(min = 3, max = 20)
    @OneToMany(mappedBy = "shift", cascade = CascadeType.ALL, orphanRemoval = true)
    private Collection<MappingWaiterShift> waiterMapping;

    /**
     * Notice there is no cascading persist, since the usage pattern is storing the order (which a reference to the shift)
     * this is also why there is no addOrder() method
     */
    @OneToMany(mappedBy = "shift", cascade = {CascadeType.REMOVE, CascadeType.REFRESH})
    private Collection<Order> orders;

    public Collection<Bartender> getBartender() {
        return JPAUtils.wrapForStream(bartenderMappings).stream().map(m->m.getBartender()).collect(Collectors.toList());
    }

    public Collection<Host> getHosts() {
        return JPAUtils.wrapForStream(hostMapping).stream().map(m->m.getHost()).collect(Collectors.toList());
    }

    public Collection<Waiter> getWaiters() {
        return JPAUtils.wrapForStream(waiterMapping).stream().map(m->m.getWaiter()).collect(Collectors.toList());
    }

    public Collection<MappingBartenderShift> getBartenderMappings() {
        return bartenderMappings = JPAUtils.initCollection(bartenderMappings);
    }

    public Collection<MappingHostShift> getHostMapping() {
        return hostMapping = JPAUtils.initCollection(hostMapping);
    }

    public Collection<MappingWaiterShift> getWaiterMapping() {
        return waiterMapping =  JPAUtils.initCollection(waiterMapping);
    }

    public Collection<Order> getOrders() {
        return orders = JPAUtils.wrapForStream(orders);
    }

    public Shift addBartender(Bartender bartender) {
        getBartenderMappings().add(new MappingBartenderShift().setBartender(bartender).setShift(this));
        return this;
    }

    public Shift addWaiter(Waiter waiter) {
        getWaiterMapping().add(new MappingWaiterShift().setWaiter(waiter).setShift(this));
        return this;
    }

    public Shift addHost(Host host) {
        getHostMapping().add(new MappingHostShift().setHost(host).setShift(this));
        return this;
    }


}
