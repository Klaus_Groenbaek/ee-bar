package dk.groenbaek.ee.resturant.jpa.dao;

import dk.groenbaek.ee.resturant.jpa.entity.Shift;
import org.eclipse.persistence.config.QueryHints;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author Klaus Groenbaek
 *         Created 22/05/2017.
 */
public class ShiftRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public Shift getShiftByUniqueId(String uniqueId) {
        return doSingleResultQuery(entityManager.createQuery("select s from Shift s where s.uniqueId = :uniqueId", Shift.class).setParameter("uniqueId", uniqueId));
    }

    public Shift batchFetchByUniqueId(String uniqueId) {
        TypedQuery<Shift> query = entityManager.createQuery("select s from Shift s where s.uniqueId = :uniqueId", Shift.class).setParameter("uniqueId", uniqueId);
        query.setHint(QueryHints.BATCH, "s.bartenderMappings");
        query.setHint(QueryHints.BATCH, "s.waiterMapping");
        query.setHint(QueryHints.BATCH, "s.hostMapping");
        query.setHint(QueryHints.BATCH, "s.orders");
        query.setHint(QueryHints.BATCH, "s.orders.lineItems");
        query.setHint(QueryHints.BATCH, "s.orders.lineItems.product");
        return doSingleResultQuery(query);
    }

    public Shift joinFetchByUniqueId(String uniqueId) {
        TypedQuery<Shift> query = entityManager.createQuery("select s from Shift s join fetch s.bartenderMappings b join fetch s.waiterMapping w join fetch s.hostMapping h join fetch s.orders o join fetch o.lineItems ol join fetch ol.product where s.uniqueId = :uniqueId", Shift.class).setParameter("uniqueId", uniqueId);
        return doSingleResultQuery(query);
    }

    public Shift joinFetchByUniqueId2(String uniqueId) {
        TypedQuery<Shift> query = entityManager.createQuery("select s from Shift s where s.uniqueId = :uniqueId", Shift.class).setParameter("uniqueId", uniqueId);
        query.setHint(QueryHints.FETCH, "s.bartenderMappings");
        query.setHint(QueryHints.FETCH, "s.waiterMapping");
        query.setHint(QueryHints.FETCH, "s.hostMapping");
        query.setHint(QueryHints.FETCH, "s.orders");
        query.setHint(QueryHints.FETCH, "s.orders.lineItems");
        query.setHint(QueryHints.FETCH, "s.orders.lineItems.product");
        return doSingleResultQuery(query);
    }

    private Shift doSingleResultQuery(TypedQuery<Shift> query) {
        List<Shift> shifts = query.getResultList();

        if (shifts.isEmpty()) {
            return null;
        } else {
            return shifts.get(0);
        }
    }

}
