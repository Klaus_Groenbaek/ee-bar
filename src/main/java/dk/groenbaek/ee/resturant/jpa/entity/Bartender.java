package dk.groenbaek.ee.resturant.jpa.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.Period;
import java.util.Collection;

/**
 * @author Klaus Groenbaek
 *         Created 20/05/2017.
 */
@Entity
@DiscriminatorValue("B")
@Getter
@Setter
public class Bartender extends Employee {

    @Basic(optional = false)
    @Column(nullable = false)
    private double multiplier = 1d;

    @Basic(optional = false)
    @Column(nullable = false)
    @Setter(AccessLevel.PRIVATE)
    private long secondsWorked;

    /**
     * No Merge cascading. If the list of shifts are loaded we don't want to run through them every time we merge a Bartender
     */
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.REFRESH})
    private Collection<MappingBartenderShift> shifts;

    @OneToMany(cascade = CascadeType.REFRESH)
    private Collection<Order> orders;

    /**
     * Age validation before insert.
     * For some reason Bean Validation using @AssertTrue will not work, possible an bug in EL, https://www.eclipse.org/forums/index.php?t=msg&th=1077658&goto=1732958&
     */
    @PrePersist
    public void checkAge() {
        LocalDate dob = getDob();
        Period period = Period.between(dob, LocalDate.now());
        if (period.getYears() < 21) {
            throw new IllegalStateException("This employee is not old enough to be a bartender, a minimum age of 21 is required");
        }
    }


    public void addSecondsWorked(long secondsWorked) {
        this.secondsWorked += secondsWorked;
    }

}
