package dk.groenbaek.ee.resturant.jpa.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;

/**
 * @author Klaus Groenbaek
 *         Created 21/05/2017.
 */
@Entity
@Setter
@Getter
@Accessors(chain = true)
@Table(name = "MAPPING_HOST_SHIFT",  uniqueConstraints = @UniqueConstraint(columnNames = {"HOST_ID", "SHIFT_ID"}))
public class MappingHostShift extends AbstractEntityBase {

    @JoinColumn(nullable = false)
    @ManyToOne(optional = false, cascade = CascadeType.PERSIST)
    private Host host;

    @JoinColumn(nullable = false)
    @ManyToOne(optional = false)
    private Shift shift;

}
