package dk.groenbaek.ee.resturant.jpa.entity;

import dk.groenbaek.ee.resturant.jpa.EntityBaseEntityListener;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author Klaus Groenbaek
 *         Created 21/05/2017.
 */
@MappedSuperclass
@EntityListeners(EntityBaseEntityListener.class)
@Setter
@Getter
public abstract class AbstractEntityBase {

    @Id
    @Setter(AccessLevel.PRIVATE)
    @GeneratedValue
    private long id;

    /**
     * A unique id for each entity, so we avoid exposing the PK in the frontend
     */
    @Column(nullable = false, length = 36, unique = true)
    private String uniqueId;
}
