package dk.groenbaek.ee.resturant.jpa.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;

/**
 * @author Klaus Groenbaek
 *         Created 20/05/2017.
 */
@Entity
@DiscriminatorValue("W")
@Getter
@Setter
public class Waiter extends Employee {

    @Basic(optional = false)
    @Column(nullable = false)
    private int tableCapacity;

    @OneToMany
    private Collection<MappingWaiterShift> shifts;

}



