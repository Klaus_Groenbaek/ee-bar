package dk.groenbaek.ee.resturant.jpa.datagenerators;

import dk.groenbaek.ee.resturant.jpa.entity.Employee;
import lombok.SneakyThrows;

import javax.inject.Inject;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * @author Klaus Groenbaek
 *         Created 21/05/2017.
 */
public class RandomEmployeeFactory {

    @Inject
    private NameGenerator nameGenerator;

    @Inject
    private SSNGenerator ssnGenerator;

    @SneakyThrows({InstantiationException.class, IllegalAccessException.class})
    public  <T extends Employee> T createEmployee(long age, Class<T> clazz) {
        T t = clazz.newInstance();
        t
                .setName(nameGenerator.randomName())
                .setDob(LocalDate.now().minus(age, ChronoUnit.YEARS))
                .setSsn(ssnGenerator.randomSSN());

        return t;
    }

}
