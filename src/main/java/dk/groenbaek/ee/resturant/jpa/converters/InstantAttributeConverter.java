package dk.groenbaek.ee.resturant.jpa.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.time.Instant;
import java.util.Date;

/**
 * Since JPA 2.1 does not support the new Java 8 time classes, we need to convert it to a storable type
 * @author Klaus Groenbaek
 *         Created 20/05/2017.
 */
@Converter(autoApply = true)
public class InstantAttributeConverter implements AttributeConverter<Instant, Date> {

    @Override
    public Date convertToDatabaseColumn(Instant locDate) {
        return (locDate == null ? null : Date.from(locDate));
    }

    @Override
    public Instant convertToEntityAttribute(Date sqlDate) {
        return (sqlDate == null ? null : sqlDate.toInstant());
    }
}