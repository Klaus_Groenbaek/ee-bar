package dk.groenbaek.ee.resturant.jpa.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;

/**
 * @author Klaus Groenbaek
 *         Created 22/05/2017.
 */
@Entity
@Getter
@Setter
@Accessors(chain = true)
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"PRODUCT_ID", "ORDER_ID"}))
public class OrderLine extends AbstractEntityBase {

    @Basic(optional = false)
    @Column(nullable = false)
    private int quantity;

    @JoinColumn(nullable = false)
    @ManyToOne(optional = false)
    private Product product;

    @JoinColumn(nullable = false)
    @ManyToOne(optional = false)
    private Order order;


}
