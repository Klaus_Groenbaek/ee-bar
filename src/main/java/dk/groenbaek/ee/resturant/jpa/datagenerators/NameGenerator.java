package dk.groenbaek.ee.resturant.jpa.datagenerators;

import java.util.Random;

/**
 * Simple name generator combining 200 first names and last names
 * @author Klaus Groenbaek
 *         Created 21/05/2017.
 */
public class NameGenerator {

    private static final String[] firstNames = new String[]{"Noah","Emma","Liam","Olivia","William","Ava","Mason","Sophia","James","Isabella","Benjamin","Mia","Jacob","Charlotte","Michael","Abigail","Elijah","Emily","Ethan","Harper","Alexander","Amelia","Oliver","Evelyn","Daniel","Elizabeth","Lucas","Sofia","Matthew","Madison","Aiden","Avery","Jackson","Ella","Logan","Scarlett","David","Grace","Joseph","Chloe","Samuel","Victoria","Henry","Riley","Owen","Aria","Sebastian","Lily","Gabriel","Aubrey","Carter","Zoey","Jayden","Penelope","John","Lillian","Luke","Addison","Anthony","Layla","Isaac","Natalie","Dylan","Camila","Wyatt","Hannah","Andrew","Brooklyn","Joshua","Zoe","Christopher","Nora","Grayson","Leah","Jack","Savannah","Julian","Audrey","Ryan","Claire","Jaxon","Eleanor","Levi","Skylar","Nathan","Ellie","Caleb","Samantha","Hunter","Stella","Christian","Paisley","Isaiah","Violet","Thomas","Mila","Aaron","Allison","Lincoln","Alexa","Charles","Anna","Eli","Hazel","Landon","Aaliyah","Connor","Ariana","Josiah","Lucy","Jonathan","Caroline","Cameron","Sarah","Jeremiah","Genesis","Mateo","Kennedy","Adrian","Sadie","Hudson","Gabriella","Robert","Madelyn","Nicholas","Adeline","Brayden","Maya","Nolan","Autumn","Easton","Aurora","Jordan","Piper","Colton","Hailey","Evan","Arianna","Angel","Kaylee","Asher","Ruby","Dominic","Serenity","Austin","Eva","Leo","Naomi","Adam","Nevaeh","Jace","Alice","Jose","Luna","Ian","Bella","Cooper","Quinn","Gavin","Lydia","Carson","Peyton","Jaxson","Melanie","Theodore","Kylie","Jason","Aubree","Ezra","Mackenzie","Chase","Kinsley","Parker","Cora","Xavier","Julia","Kevin","Taylor","Zachary","Katherine","Tyler","Madeline","Ayden","Gianna","Elias","Eliana","Bryson","Elena","Leonardo","Vivian","Greyson","Willow","Sawyer","Reagan","Roman","Brianna","Brandon","Clara","Bentley","Faith"};
    private static final String[] lastName = new String[] {"Smith","Johnson","Williams","Jones","Brown","Davis","Miller","Wilson","Moore","Taylor","Anderson","Thomas","Jackson","White","Harris","Martin","Thompson","Garcia","Martinez","Robinson","Clark","Rodriguez","Lewis","Lee","Walker","Hall","Allen","Young","Hernandez","King","Wright","Lopez","Hill","Scott","Green","Adams","Baker","Gonzalez","Nelson","Carter","Mitchell","Perez","Roberts","Turner","Phillips","Campbell","Parker","Evans","Edwards","Collins","Stewart","Sanchez","Morris","Rogers","Reed","Cook","Morgan","Bell","Murphy","Bailey","Rivera","Cooper","Richardson","Cox","Howard","Ward","Torres","Peterson","Gray","Ramirez","James","Watson","Brooks","Kelly","Sanders","Price","Bennett","Wood","Barnes","Ross","Henderson","Coleman","Jenkins","Perry","Powell","Long","Patterson","Hughes","Flores","Washington","Butler","Simmons","Foster","Gonzales","Bryant","Alexander","Russell","Griffin","Diaz","Hayes","Myers","Ford","Hamilton","Graham","Sullivan","Wallace","Woods","Cole","West","Jordan","Owens","Reynolds","Fisher","Ellis","Harrison","Gibson","Mcdonald","Cruz","Marshall","Ortiz","Gomez","Murray","Freeman","Wells","Webb","Simpson","Stevens","Tucker","Porter","Hunter","Hicks","Crawford","Henry","Boyd","Mason","Morales","Kennedy","Warren","Dixon","Ramos","Reyes","Burns","Gordon","Shaw","Holmes","Rice","Robertson","Hunt","Black","Daniels","Palmer","Mills","Nichols","Grant","Knight","Ferguson","Rose","Stone","Hawkins","Dunn","Perkins","Hudson","Spencer","Gardner","Stephens","Payne","Pierce","Berry","Matthews","Arnold","Wagner","Willis","Ray","Watkins","Olson","Carroll","Duncan","Snyder","Hart","Cunningham","Bradley","Lane","Andrews","Ruiz","Harper","Fox","Riley","Armstrong","Carpenter","Weaver","Greene","Lawrence","Elliott","Chavez","Sims","Austin","Peters","Kelley","Franklin","Lawson"};

    private final Random rnd = new Random();

    public String randomName() {
        return firstNames[rnd.nextInt(firstNames.length)] + " " + lastName[rnd.nextInt(lastName.length)];
    }

}
