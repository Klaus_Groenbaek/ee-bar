package dk.groenbaek.ee.resturant.jpa.entity;

import dk.groenbaek.ee.resturant.jpa.JPAUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.util.Collection;

/**
 * @author Klaus Groenbaek
 *         Created 22/05/2017.
 */
@Entity
@Getter
@Setter
@Accessors(chain = true)
public class Product extends AbstractEntityBase {

    @Basic(optional = false)
    @Column(nullable = false, unique = true)
    private String name;

    @Basic(optional = false)
    @Column(nullable = false)
    private double price;

    /**
     * Time to make this product in seconds
     */
    @Basic(optional = false)
    @Column(nullable = false)
    private int timeToMake;

    @OneToMany(mappedBy = "product")
    private Collection<OrderLine> orderLines;

    public Collection<OrderLine> getOrderLines() {
        return orderLines = JPAUtils.initCollection(orderLines);
    }

}
