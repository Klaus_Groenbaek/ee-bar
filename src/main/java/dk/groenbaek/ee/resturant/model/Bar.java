package dk.groenbaek.ee.resturant.model;

import com.google.common.base.Preconditions;
import dk.groenbaek.ee.resturant.jpa.dao.ShiftRepository;
import dk.groenbaek.ee.resturant.jpa.entity.Bartender;
import dk.groenbaek.ee.resturant.jpa.entity.Order;
import dk.groenbaek.ee.resturant.jpa.entity.Shift;
import lombok.SneakyThrows;

import javax.annotation.PostConstruct;
import javax.annotation.concurrent.ThreadSafe;
import javax.ejb.*;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.Collection;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Models a Bar where customers can place orders
 *
 *
 *
 * @author Klaus Groenbaek
 *         Created 22/05/2017.
 */
@Singleton
@ThreadSafe // we want a threadsafe singleton where we controls the threads
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class Bar {

    @Inject
    private ShiftRepository shiftRepository;

    @Inject
    private OrderProcessor orderProcessor;

    @PersistenceContext
    private EntityManager entityManager;

    private volatile BarConfig config;

    @PostConstruct
    public void loadShift() {
        loadShift("evening");
    }

    public void loadShift(String shiftUniqueId) {
        Shift shift = shiftRepository.getShiftByUniqueId(shiftUniqueId);
        config = new BarConfig(shift);
    }

    @SneakyThrows(InterruptedException.class)
    @Transactional
    public void processOrder(Order order) {

        Preconditions.checkArgument(order.getId() == 0, "Orders can't have an id (be in the database)");
        BarConfig localConfig = this.config;
        Bartender bartender = localConfig.bartenderQueue.take();
        try {

            int totalTimeToMake = order.getTotalTimeToMake();
            orderProcessor.processOrder(order);

            // we need to set the bartender and shift on the order, since we need to update the bartender but not the shift we use two different approaches
            // bartender is updated and merged. There is no optimistic locking, so we assume that this is the only place data on a bartender can ever be updated
            bartender.addSecondsWorked(totalTimeToMake);
            order.setBartender(bartender);
            entityManager.merge(bartender);

            // The shift is also detached, but we do not need to update it, so we just need the EntityManager to create a (managed) reference with the correct ID
            order.setShift(entityManager.getReference(Shift.class, localConfig.shift.getId()));
            entityManager.persist(order);

        } finally {
            // bartender is done working add him back to the pool
            localConfig.bartenderQueue.add(bartender);
        }
    }

    public String getShiftId() {
        return config.shift.getUniqueId();
    }

    public int getAvailableBartenders() {
        return config.bartenderQueue.size();
    }


    /**
     * Groups the Runtime configuration for the given bar shift, so we can load a new shift as an atomic action
     */
    private static class BarConfig {
        private final Shift shift;
        private final ArrayBlockingQueue<Bartender> bartenderQueue;

        private BarConfig(Shift shift) {
            this.shift = shift;
            Collection<Bartender> bartenders = shift.getBartender();
            this.bartenderQueue = new ArrayBlockingQueue<>(bartenders.size());
            this.bartenderQueue.addAll(bartenders);

        }
    }

}
