package dk.groenbaek.ee.resturant.model;

import dk.groenbaek.ee.resturant.jpa.entity.Order;

/**
 * Interface that specify how orders are processed
 * @author Klaus Groenbaek
 *         Created 22/05/2017.
 */
public interface OrderProcessor {
    void processOrder(Order order);
}
