package dk.groenbaek.ee.resturant.model;

import dk.groenbaek.ee.resturant.jpa.entity.Order;
import lombok.SneakyThrows;

import javax.inject.Singleton;

/**
 * Implementation of order processor that simulates that it takes time to service an order
 * @author Klaus Groenbaek
 *         Created 22/05/2017.
 */
@Singleton
public class SleepOrderProcessor implements OrderProcessor {

    @Override
    @SneakyThrows(InterruptedException.class)
    public void processOrder(Order order) {
        // factor we used to speedup the order processing so we can run simulations faster, we use local variable so we can change it in the debugger
        int speedUpBy = 100;

        int totalTimeToMake = order.getTotalTimeToMake();
        double adjustedTime = totalTimeToMake * 1000.0 / speedUpBy;
        Thread.sleep((long) adjustedTime);
    }
}
