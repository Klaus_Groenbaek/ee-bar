package dk.groenbaek.ee.resturant.web;

import dk.groenbaek.ee.resturant.jpa.entity.Bartender;
import dk.groenbaek.ee.resturant.jpa.entity.Manager;
import dk.groenbaek.ee.resturant.jpa.entity.Shift;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * @author Klaus Groenbaek
 *         Created 21/05/2017.
 */
@Path("/debug")
public class DebugController {


    @PersistenceContext
    EntityManager entityManager;

    @GET
    @Transactional
    public String debug() {

        Shift shift = new Shift().setStart(Instant.now().minus(1, ChronoUnit.HOURS)).setStop(Instant.now())
                .setManager(createManager(30))
                .addBartender(createBartender(25));

        entityManager.persist(shift);

        return "OK";

    }

    protected Bartender createBartender(int age) {
        Bartender bartender = new Bartender();
        bartender.setDob(LocalDate.now().minus(age, ChronoUnit.YEARS));
        bartender.setName("Michael");
        bartender.setSsn("1");
        return bartender;
    }

    protected Manager createManager(int age) {
        Manager bartender = new Manager();
        bartender.setDob(LocalDate.now().minus(age, ChronoUnit.YEARS));
        bartender.setName("Peter");
        bartender.setSsn("1");
        return bartender;
    }
}
