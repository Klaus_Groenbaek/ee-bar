package dk.groenbaek.ee.resturant;

import dk.groenbaek.ee.resturant.jpa.dao.ProductRepository;
import dk.groenbaek.ee.resturant.jpa.datagenerators.RandomEmployeeFactory;
import dk.groenbaek.ee.resturant.jpa.entity.Order;
import dk.groenbaek.ee.resturant.jpa.entity.Product;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import java.util.List;

/**
 * @author Klaus Groenbaek
 *         Created 21/05/2017.
 */
@RunWith(Arquillian.class)
public abstract class AbstractTestBase {

    @Inject
    private ProductRepository productRepository;

    @Deployment
    public static Archive<?> createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackages(true, "dk.groenbaek.ee.resturant")
                .addAsResource("Derby_persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource("beans.xml");

    }

    @PersistenceUnit
    protected EntityManagerFactory emf;

    @PersistenceContext(name = "barPU")
    protected EntityManager entityManager;

    // We need a user transaction, since we use an Application managed EntityManager and are not allowed to call EntityManager.getTransaction()
    @Inject
    protected UserTransaction utx;

    @Inject
    protected RandomEmployeeFactory employeeFactory;

    protected Order createOrder() {
        Order order = new Order();
        List<Product> products = productRepository.all();
        for (Product product : products) {
            order.addProduct(product, 1);
        }
        return order;
    }
}
