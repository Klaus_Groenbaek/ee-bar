package dk.groenbaek.ee.resturant.jpa.entity;

import dk.groenbaek.ee.resturant.AbstractTestBase;
import org.junit.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.junit.Assert.assertEquals;

/**
 * @author Klaus Groenbaek
 *         Created 21/05/2017.
 */
public class ShiftTest extends AbstractTestBase {

    @Test
    public void notEnoughWaiters() throws Exception{

        Shift shift = new Shift().setStart(Instant.now().minus(1, ChronoUnit.HOURS)).setStop(Instant.now())
                .setManager(employeeFactory.createEmployee(30, Manager.class))
                .addBartender(employeeFactory.createEmployee(25, Bartender.class))
                .addHost(employeeFactory.createEmployee(18, Host.class));

        try {
            utx.begin();
            entityManager.joinTransaction();
            entityManager.persist(shift);
            utx.commit();
        } catch (IllegalStateException e) {
            assertEquals("waiterMapping: size must be between 3 and 20", e.getMessage());
            utx.rollback();
        }
        catch (RuntimeException e) {
            utx.rollback();
        }
    }

}
