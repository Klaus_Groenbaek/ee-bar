package dk.groenbaek.ee.resturant.jpa.entity;

import dk.groenbaek.ee.resturant.AbstractTestBase;
import org.junit.Test;

import javax.transaction.*;

/**
 * Low-level test direct against JPA, where we manage the transaction
 * @author Klaus Groenbaek
 *         Created 20/05/2017.
 */
public class BartenderTest extends AbstractTestBase {

    @Test(expected = IllegalStateException.class)
    public void testTooYong() throws Exception {

        persistBartender(20);
    }

    @Test
    public void testOldEnough() throws Exception {
        persistBartender(21);
    }

    private void persistBartender(int age) throws NotSupportedException, SystemException, HeuristicMixedException, HeuristicRollbackException, RollbackException {
        Bartender bartender = employeeFactory.createEmployee(age, Bartender.class);

        try {
            utx.begin();
            entityManager.joinTransaction();
            entityManager.persist(bartender);
            utx.commit();
        } catch (RuntimeException e) {
            // If the transaction was completely managed as with a @Transactional bean, the EE Container would automatically rollback all RuntimeExceptions,
            // however since we use a UserTransaction directly we have to do it ourselves
            utx.rollback();
            throw e;
        }
    }


}
