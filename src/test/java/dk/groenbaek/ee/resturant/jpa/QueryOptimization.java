package dk.groenbaek.ee.resturant.jpa;

import com.google.common.collect.Lists;
import dk.groenbaek.ee.resturant.AbstractTestBase;
import dk.groenbaek.ee.resturant.ebj.TestOrderProcessor;
import dk.groenbaek.ee.resturant.jpa.dao.ShiftRepository;
import dk.groenbaek.ee.resturant.jpa.datagenerators.DataPopulator;
import dk.groenbaek.ee.resturant.jpa.datagenerators.UniqueIdGenerator;
import dk.groenbaek.ee.resturant.jpa.entity.Order;
import dk.groenbaek.ee.resturant.jpa.entity.Shift;
import dk.groenbaek.ee.resturant.model.Bar;

import lombok.SneakyThrows;
import lombok.extern.java.Log;
import org.junit.Before;
import org.junit.Test;

import javax.inject.Inject;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * This test is only here to show the different SQL generated,
 * remember to enable query logging in Derby_persistence.xml
 * Also ensure that Shared caching is disabled, otherwise it may load nothing from the DB
 *
 * @author Klaus Groenbaek
 *         Created 22/05/2017.
 */
@Log
public class QueryOptimization extends AbstractTestBase {

    @Inject
    UniqueIdGenerator idGenerator;

    @Inject
    ShiftRepository shiftRepository;

    @Inject
    TestOrderProcessor orderProcessor;

    @Inject
    DataPopulator dataPopulator;

    @Inject
    Bar bar;

    private String shiftUniqueId;

    @Before
    @SneakyThrows
    public void addOrders() {

        try {
            utx.begin();
            entityManager.joinTransaction();
            List<Shift> shifts = dataPopulator.createShifts(Lists.newArrayList(new DataPopulator.ShiftConfiguration(idGenerator.createUniqueId(), 4, 10, 2, 10, 10)));
            Shift shift = shifts.get(0);
            utx.commit();

            shiftUniqueId = shift.getUniqueId();
        } catch (RuntimeException e) {
            // If the transaction was completely managed as with a @Transactional bean, the EE Container would automatically rollback all RuntimeExceptions,
            // however since we use a UserTransaction directly we have to do it ourselves
            utx.rollback();
            throw e;
        }

        bar.loadShift(shiftUniqueId);
        for (int i=0; i < 50; i++) {
            orderProcessor.releaseWaiting();
            bar.processOrder(createOrder());
        }

    }

    @Test
    public void loadShift() {
        log.info("--SHIFT--");
        Shift evening = shiftRepository.getShiftByUniqueId(shiftUniqueId);
        log.info("--SHIFT--");
    }


    @Test
    public void iterateCollections() {
        log.info("--SHIFT AND COLLECTIONS--");

        Shift shift = shiftRepository.getShiftByUniqueId(shiftUniqueId);
        shift.getWaiters();
        shift.getBartender();
        shift.getHosts();

        log.info("Total earnings " + shift.getOrders().stream().mapToDouble(o-> o.getTotalPrice()).sum());
        log.info("--SHIFT AND COLLECTIONS--");
    }

    @Test
    public void joinFetch() {
        log.info("--SHIFT-- JOIN FETCH");
        Shift shift = shiftRepository.joinFetchByUniqueId2(shiftUniqueId);
        log.info("Total earnings " + shift.getOrders().stream().mapToDouble(o-> o.getTotalPrice()).sum());
        log.info("--SHIFT-- JOIN FETCH");
    }

    @Test
    public void batchFetch() {
        log.info("--SHIFT-- BATCH FETCH");
        Shift shift = shiftRepository.batchFetchByUniqueId(shiftUniqueId);
        log.info("Total earnings " + shift.getOrders().stream().mapToDouble(o-> o.getTotalPrice()).sum());
        log.info("--SHIFT-- BATCH FETCH");
    }


}
