package dk.groenbaek.ee.resturant.ebj;

import dk.groenbaek.ee.resturant.jpa.entity.Order;
import dk.groenbaek.ee.resturant.model.OrderProcessor;
import lombok.Getter;
import lombok.SneakyThrows;

import javax.enterprise.inject.Alternative;
import javax.inject.Singleton;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * A No Operation version of the orderProcessor to be used in tests
 * @author Klaus Groenbaek
 *         Created 22/05/2017.
 */
@Alternative
@Singleton
public class TestOrderProcessor implements OrderProcessor {

    private final AtomicInteger waiting = new AtomicInteger(0);
    private final Semaphore latch = new Semaphore(0);

    @SneakyThrows(InterruptedException.class)
    @Override
    public void processOrder(Order order) {
        waiting.incrementAndGet();
        latch.acquire();
        waiting.decrementAndGet();
    }




    public void releaseWaiting() {
        latch.release();
    }

    public int getOrdersWaiting() {
        return waiting.get();
    }

}
