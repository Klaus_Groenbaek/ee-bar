package dk.groenbaek.ee.resturant.ebj;

import dk.groenbaek.ee.resturant.AbstractTestBase;
import dk.groenbaek.ee.resturant.jpa.dao.ProductRepository;
import dk.groenbaek.ee.resturant.jpa.dao.ShiftRepository;
import dk.groenbaek.ee.resturant.jpa.entity.Order;
import dk.groenbaek.ee.resturant.jpa.entity.Product;
import dk.groenbaek.ee.resturant.model.Bar;
import org.junit.Test;

import javax.inject.Inject;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.Assert.*;

/**
 * @author Klaus Groenbaek
 *         Created 22/05/2017.
 */
public class BarTest extends AbstractTestBase {


    @Inject
    private Bar bar;

    @Inject
    private ShiftRepository shiftRepository;

    @Inject
    private TestOrderProcessor orderProcessor;

    @Test
    public void testBar() {

        Order order = createOrder();

        orderProcessor.releaseWaiting();
        bar.processOrder(order);

        assertNotEquals("primary key should have been auto generated",0, order.getId());
        assertNotNull("An unique ID should have been assigned", order.getUniqueId());
    }


    /**
     * WARNING: This test/code is written in a way that will cause it to fail if Shared cache is enabled. This is because the lazy-loaded list of orders
     * on a Shift may be cached from when it was first loaded, and since we didn't maintain the two way reference in code the cached entity is out of sync with
     * the database
     */
    @Test
    public void testShiftSwapping()  {

        String beginningShift = bar.getShiftId();
        try {
            assertEquals("A previous test forgot to set the shift bark to evening", "evening", beginningShift);

            int eveningBefore = shiftRepository.getShiftByUniqueId(beginningShift).getOrders().size();
            int lunchBefore = shiftRepository.getShiftByUniqueId("lunch").getOrders().size();

            orderProcessor.releaseWaiting();
            bar.processOrder(createOrder());

            assertEquals("Evening should get one more order", eveningBefore+1, shiftRepository.getShiftByUniqueId(beginningShift).getOrders().size());
            assertEquals("Lunch should not be affected", lunchBefore, shiftRepository.getShiftByUniqueId("lunch").getOrders().size());

            bar.loadShift("lunch");
            orderProcessor.releaseWaiting();
            bar.processOrder(createOrder());

            assertEquals("Evening should get one more order", eveningBefore+1, shiftRepository.getShiftByUniqueId(beginningShift).getOrders().size());
            assertEquals("Lunch should get one more order", lunchBefore +1, shiftRepository.getShiftByUniqueId("lunch").getOrders().size());
        } finally {
            bar.loadShift(beginningShift);
        }
    }


    /**
     * Test that if there are 3 bartenders, then the 4 order will have to wait until one of the first 3 orders completes
     */
    @Test
    public void testConcurrency() throws Exception {

        // allow for 3 orders
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        for (int i=0; i < 3; i++) {
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    bar.processOrder(createOrder());
                }

            });
        }

        int count = 0;
        while(orderProcessor.getOrdersWaiting() != 3 && count++ < 100) {
            Thread.sleep(100);
        }
        if  (count >= 100) {
            fail("should be waiting");
        }

        AtomicReference<Thread> threadRef = new AtomicReference<>();
        CountDownLatch latch = new CountDownLatch(1);
        // this thread should wait for a bartender and not the order processor
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                threadRef.set(Thread.currentThread());
                latch.countDown();
                bar.processOrder(createOrder());
            }
        });

        assertTrue("Latch should be called", latch.await(10, TimeUnit.SECONDS));

        Thread waitingThread = threadRef.get();
        // use the Thread management bean to check the state of the beans
        ThreadInfo threadInfo = waitForWaitingThread(waitingThread.getId());
        String lockName = threadInfo.getLockName();
        assertTrue("invalid lock name: " + lockName, lockName.startsWith("java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject"));
        assertEquals("The 4th thread should be waiting for a bartender", 3, orderProcessor.getOrdersWaiting());

        // now allow one of the orders to be processed
        orderProcessor.releaseWaiting();
        // we can't know when the 4th thread will start running, we only know that at some point it will block on the semaphore instead of the array blocking queue
        while (lockName.startsWith("java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject")) {
            Thread.sleep(100);
            threadInfo = waitForWaitingThread(waitingThread.getId());
            lockName = threadInfo.getLockName();
        }
        assertTrue("invalid lock name: " + lockName, lockName.startsWith("java.util.concurrent.Semaphore$NonfairSync"));
        assertEquals("The 4th thread should be waiting for a bartender", 3, orderProcessor.getOrdersWaiting());

        // allow all to finish
        orderProcessor.releaseWaiting();
        orderProcessor.releaseWaiting();
        orderProcessor.releaseWaiting();

        executorService.shutdown();
        assertTrue("Not all threads stopped in time", executorService.awaitTermination(10, TimeUnit.SECONDS));
    }

    private ThreadInfo waitForWaitingThread(long threadId) throws InterruptedException {
        ThreadMXBean bean = ManagementFactory.getThreadMXBean();
        while (true) {
            Thread.sleep(100);
            ThreadInfo threadInfo = bean.getThreadInfo(threadId);
            if (threadInfo.getThreadState() == Thread.State.WAITING) {
                return threadInfo;
            }
        }
    }


}
