# README #

### EE-BAR ###

A EE sample application using the following technologies

* JPA
* Session Beans and Managed Beans
* Message Driven Bean (JMS)
* JAX-RS
* Concurrency Utilities

### Build environment ###
Project is build by Gradle, you need to set JAVA_HOME environment variable to a JDK to run **(./)gradlew build**

### Deployment ###
 


